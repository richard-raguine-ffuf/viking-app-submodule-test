package com.example.samplesubmodule

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import com.example.dealerhelp.DealerHelpOverview

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<AppCompatButton>(R.id.btn_main)
        button.setOnClickListener { v ->
            startActivity(Intent(this, DealerHelpOverview::class.java))
        }
    }
}
