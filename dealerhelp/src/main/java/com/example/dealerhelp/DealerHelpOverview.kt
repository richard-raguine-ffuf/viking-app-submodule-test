package com.example.dealerhelp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by ffuf on 2019-01-17.
 * @author mark.magno@ffuf.de
 */

class DealerHelpOverview : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dealer_help_overview)
    }
}